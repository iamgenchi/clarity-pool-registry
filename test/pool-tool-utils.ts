import {
  AccountsApi,
  AddressTransactionsListResponse,
  Configuration,
  InfoApi,
  SmartContractsApi,
  TransactionsApi,
} from "@stacks/blockchain-api-client";
import {
  AddressHashMode,
  addressHashModeToVersion,
  ClarityType,
  ClarityValue,
  cvToHex,
  cvToJSON,
  cvToString,
  hexToCV,
  listCV,
  makeContractCall,
  OptionalCV,
  PrincipalCV,
  someCV,
  SomeCV,
  standardPrincipalCV,
  TupleCV,
  tupleCV,
  UIntCV,
  uintCV,
} from "@stacks/transactions";
import { StacksNetwork } from "@stacks/network";
import { readFileSync, writeFileSync } from "fs";
import {
  handleTransaction,
  mainnet,
  mocknet,
  network,
  STACKS_API_URL,
  timeout,
} from "./deploy";
import { address } from "bitcoinjs-lib";
import { StackingClient } from "@stacks/stacking";
import { poxAddrCV, poxCVToBtcAddress } from "./utils-pox-addr";

const fetch = require("node-fetch");
import BigNum from "bn.js";

interface DelegationDataCV {
  delegatedTo?: string;
  amountUstx?: BigNum;
  untilBurnHt?: number;
  poxAddress?: string;
}
const delegationDataCV = (cv: ClarityValue): DelegationDataCV | undefined => {
  console.log(JSON.stringify(cv));
  if (cv.type === ClarityType.OptionalSome) {
    const tupleData = ((cv as SomeCV).value as TupleCV).data;
    const untilBurnHtCV = tupleData["until-burn-ht"];
    const poxAddressCV = tupleData["pox-addr"];
    return {
      delegatedTo: cvToString(tupleData["delegated-to"]),
      amountUstx: (tupleData["amount-ustx"] as UIntCV).value,
      untilBurnHt:
        untilBurnHtCV.type === ClarityType.OptionalSome
          ? (untilBurnHtCV.value as UIntCV).value.toNumber()
          : undefined,
      poxAddress:
        poxAddressCV.type === ClarityType.OptionalSome
          ? poxCVToBtcAddress(poxAddressCV.value as TupleCV)
          : undefined,
    };
  } else {
    return undefined;
  }
};

const config = new Configuration({
  basePath: STACKS_API_URL,
  fetchApi: fetch,
});
export const infoApi = new InfoApi(config);
const contractsApi = new SmartContractsApi(
  new Configuration({
    basePath: STACKS_API_URL,
    fetchApi: fetch,
  })
);
const transcationsApi = new TransactionsApi(config);
const accountsApi = new AccountsApi(config);
const limit = 30;

export const poxContractAddress = mainnet
  ? "SP000000000000000000002Q6VF78"
  : "ST000000000000000000002AMW42H";

export const poolToolContractV0 = mainnet
  ? {
      address: "SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60",
      name: "pool-tool",
    }
  : {
      address: "ST314JC8J24YWNVAEJJHQXS5Q4S9DX1FW5Z9DK9NT",
      name: "pool-tool",
    };

export const poolToolContract = mainnet
  ? {
      address: "SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60",
      name: "pool-tool-v1",
    }
  : {
      address: "ST314JC8J24YWNVAEJJHQXS5Q4S9DX1FW5Z9DK9NT",
      name: "pool-tool-v1",
    };

export function getBtcAddress(checksumHex: string, versionHex: string) {
  const checksum = Buffer.from(checksumHex, "hex");
  const version = Buffer.from(versionHex, "hex");
  const btcAddress = address.toBase58Check(
    checksum,
    new BigNum(version).toNumber()
  );
  return btcAddress;
}

export function stackersToCycle(
  stackers: { rewardCycle: number; amount: number; stacker: string }[],
  cycle: number
) {
  const members = stackers.filter((s: any) => (s.rewardCycle + s.lockingPeriod > cycle));
  return {
    cycle,
    members,
    count: members.length,
    total: members.reduce((sum, member) => sum + member.amount, 0),
  };
}

export function getStackersFromPoolTool(
  contract: string,
  network: StacksNetwork
) {
  const txsFilePath = `acc-txs-${network.chainId}-${contract}.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());
  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );
  let stackers: any[] = [];
  for (const tx of txs) {
    let stackersInTx: any[] = [];
    switch (tx.contract_call.function_name) {
      case "delegate-stack-stx":
      case "delegate-stack-stx-and-commit":
        const lockingPeriod = (hexToCV(
          tx.contract_call.function_args[3].hex
        ) as any).value.toNumber();
        const rewardCycle = (hexToCV(
          tx.contract_call.function_args[4].hex
        ) as any).value.toNumber();
        const events = cvToJSON(hexToCV(tx.tx_result.hex)).value.value[
          "stack-result"
        ].value;
        stackersInTx = events.map((e: any) => [
          e.success,
          e.value.value["lock-amount"].value,
          e.value.value["stacker"].value,
          e.value.value["unlock-burn-height"].value,
          rewardCycle,
          lockingPeriod,
        ]);
        /*
        detailsList = tx.events.map((e: any) => [
          e.event_type,
          e.stx_lock_event.locked_address,
          e.stx_lock_event.locked_amount / 1000000,
          e.stx_lock_event.unlock_height,
        ]);
        */
        break;
      default:
        stackersInTx = [];
    }
    /*
    detailsList.forEach((details) => {
      console.log(
        Array.of(
          tx.burn_block_time_iso,
          tx.block_height,
          tx.contract_call.function_name,
          tx.sender_address,
          tx.contract_call.contract_id,
          ...details
        ).join(",")
      );
    });
    */

    stackers = stackers.concat(
      stackersInTx.map((details) => {
        return {
          timestamp: tx.burn_block_time_iso,
          blockheight: tx.block_height,
          txid: tx.tx_id,
          success: details[0],
          amount: details[1],
          stacker: details[2],
          unlock: details[3],
          rewardCycle: details[4],
          lockingPeriod: details[5],
        };
      })
    );
  }
  return stackers;
}

export function logPoxContractCSV(network: StacksNetwork) {
  const txsFilePath = `acc-txs-${network.chainId}-${poxContractAddress}.pox.json`;
  let txs = JSON.parse(readFileSync(txsFilePath).toString());

  txs = txs.filter(
    (t: any) => t.tx_status === "success" && t.tx_type === "contract_call"
  );
  console.log(
    Array.of(
      "time",
      "block",
      "function",
      "sender",
      "amount",
      "pool-admin",
      "block-height",
      "locking-cycles",
      "pox-address"
    ).join(",")
  );
  for (const tx of txs) {
    let details: any[] = [];
    switch (tx.contract_call.function_name) {
      case "stack-stx":
        details = [
          tx.contract_call.function_args[0].repr.substr(1) / 1000000,
          "",
          tx.contract_call.function_args[2].repr.substr(1),
          tx.contract_call.function_args[3].repr.substr(1),
          tx.contract_call.function_args[1].repr,
        ];
        break;
      case "delegate-stx":
        details = [
          tx.contract_call.function_args[0].repr.substr(1) / 1000000,
          tx.contract_call.function_args[1].repr,
          "",
          tx.contract_call.function_args[3].repr,
          tx.contract_call.function_args[2].repr,
        ];
        break;
      case "delegate-stack-stx":
        details = [
          tx.contract_call.function_args[1].repr.substr(1) / 1000000,
          tx.contract_call.function_args[0].repr,
          tx.contract_call.function_args[3].repr.substr(1),
          tx.contract_call.function_args[4].repr.substr(1),
          tx.contract_call.function_args[2].repr,
        ];
        break;
      case "allow-contract-caller":
        details = [tx.contract_call.function_args[0].repr];
        break;
      case "disallow-contract-caller":
        details = [tx.contract_call.function_args[0].repr];
        break;
      case "revoke-delegate-stx":
      default:
        details = [];
    }
    console.log(
      Array.of(
        tx.burn_block_time_iso,
        tx.block_height,
        tx.contract_call.function_name,
        tx.sender_address,
        ...details
      ).join(",")
    );
  }
}

export async function getFilteredDelegationStates(
  admin: { stacks: string },
  minUntilBurnHt: number | undefined = undefined,
  onlyUnstackedMembers: boolean
) {
  const allStates = getDelegationStates("");
  let delegationStates = allStates.find(
    (poolData: any) => poolData.delegatee === admin.stacks
  )?.stackerStates;

  delegationStates = delegationStates?.filter(
    // only log good members
    (s: any) =>
      s.stacker !== admin.stacks && // ignore the admin
      s.data &&
      s.data.delegatedTo === admin.stacks && // ignore members stacking to other pools
      (!onlyUnstackedMembers || !s.status.stacked) && // ignore members that are already stacked
      (!minUntilBurnHt ||
        !s.data.untilBurnHt ||
        s.data.untilBurnHt > minUntilBurnHt) // ignore members with expiring delegation
  );
  return delegationStates;
}

export async function logDelegationStatesCSV(
  admin: { stacks: string },
  onlyUnstackedMembers: boolean = true
) {
  const delegationStates = await getFilteredDelegationStates(
    admin,
    undefined,
    onlyUnstackedMembers
  );
  if (delegationStates) {
    delegationStates.forEach((s: any) => {
      console.log(
        `${s.stacker}, ${s.status.stacked}, ${parseInt(
          s.data.amountUstx,
          16
        ).toString()}, ${admin.stacks}, ${s.data.untilBurnHt}`
      );
    });
  }
}

export async function downloadPoolToolTxs(network: StacksNetwork) {
  return downloadAccountTxs(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network
  );
}

export async function downloadPoolToolV0Txs(network: StacksNetwork) {
  return downloadAccountTxs(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
}

export async function downloadPoxTxs(network: StacksNetwork) {
  return downloadAccountTxs(`${poxContractAddress}.pox`, network);
}

export async function downloadXverseTxs(network: StacksNetwork) {
  return downloadAccountTxs(
    "SPXVRSEH2BKSXAEJ00F1BY562P45D5ERPSKR4Q33.xverse-pool-v1",
    network
  );
}
export async function downloadAccountTxs(
  principal: string,
  network: StacksNetwork
) {
  const txsFilePath = `acc-txs-${network.chainId}-${principal}.json`;
  let txs: any[] = [];
  try {
    txs = JSON.parse(readFileSync(txsFilePath).toString());
  } catch (e) {
    console.log(e);
    txs = [];
  }
  const lastBlock = txs.reduce(
    (seenBlock: number, tx) => Math.max(seenBlock, tx.block_height),
    0
  );
  console.log({ lastBlock });
  let results: AddressTransactionsListResponse;
  let offset = 0;
  let downloadedTxs: any[] = [];
  let newTxs: any[] = [];
  do {
    results = await accountsApi.getAccountTransactions({
      principal,
      offset,
      limit,
    });
    newTxs = results.results.filter(
      (newTx: any) => txs.findIndex((t) => t.tx_id === newTx.tx_id) < 0
    );
    downloadedTxs = downloadedTxs.concat(newTxs);
    offset = offset + results.results.length;
  } while (results.results.length === limit && newTxs.length > 0);
  console.log(txs.length, downloadedTxs.length);
  txs = downloadedTxs.concat(txs);
  writeFileSync(txsFilePath, JSON.stringify(txs));
  return txs;
}

export function getDelegationStates(
  poolAdmin = "SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60"
) {
  const statesFilePath = `delegation-states-${network.chainId}-${poolAdmin}.json`;
  return JSON.parse(readFileSync(statesFilePath).toString()) as {
    delegatee: string;
    stackerStates: {
      stacker: string;
      data: DelegationDataCV;
      status: any;
    }[];
  }[];
}

export async function writeDelegationStates(
  txs: any[],
  poolAdmin = "SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60"
) {
  const statesFilePath = `delegation-states-${network.chainId}-${poolAdmin}.json`;
  const summaryFilePath = `summary-${network.chainId}-${poolAdmin}.json`;

  const delegateStxTxs = txs.filter(
    (tx: any) =>
      tx.tx_type === "contract_call" &&
      tx.contract_call.function_name === "delegate-stx" &&
      tx.tx_status === "success" &&
      (!poolAdmin || tx.contract_call.function_args[1].repr === poolAdmin)
  );
  console.log(delegateStxTxs.length);

  const pools = delegateStxTxs.reduce((poolList: any, delegateStxTx: any) => {
    const delegatee = delegateStxTx.contract_call.function_args[1].repr;
    const stacker = delegateStxTx.sender_address;
    const amount = parseInt(
      delegateStxTx.contract_call.function_args[0].repr.substr(1)
    );
    if (poolList[delegatee]) {
      poolList[delegatee].total += amount;
      poolList[delegatee].count += 1;
      poolList[delegatee].stackers[stacker] = { amount };
    } else {
      const stackers: any = {};
      stackers[stacker] = { amount };
      poolList[delegatee] = {
        total: amount,
        count: 1,
        stackers,
      };
    }
    return poolList;
  }, {});

  let existingDelegationStates: any;
  try {
    existingDelegationStates = getDelegationStates(poolAdmin);
  } catch (e) {
    console.log(e.toString());
    existingDelegationStates = [];
  }

  const delegationStates = await Promise.all(
    Object.keys(pools).map(async (delegatee) => {
      const existingStatesForPool = existingDelegationStates.find(
        (s: any) => s.delegatee == delegatee
      );
      const mapEntryCalls = Object.keys(pools[delegatee].stackers).map(
        async (stacker) => {
          console.log(stacker, poxContractAddress, delegatee);
          const key = cvToHex(
            tupleCV({ stacker: standardPrincipalCV(stacker) })
          );
          console.log(key);

          try {
            const stackingClient = new StackingClient(stacker, network);
            const status = await stackingClient.getStatus();
            console.log(status);
            const state = await contractsApi.getContractDataMapEntry({
              contractAddress: poxContractAddress,
              contractName: "pox",
              mapName: "delegation-state",
              key,
              proof: 0,
            });
            return {
              stacker: stacker,
              data: delegationDataCV(hexToCV(state.data)),
              status,
            };
          } catch (e) {
            return e;
          }
        }
      );
      const stackerStates: {
        stacker: string;
        data: DelegationDataCV;
        status: any;
      }[] = [];
      for (const call of mapEntryCalls) {
        stackerStates.push(await call);
        await timeout(1000);
        console.log(stackerStates.length);
      }
      return { delegatee, stackerStates };
    })
  );

  const isStillPoolMember = (
    delegatee: string,
    stackerState: { data: DelegationDataCV }
  ) => stackerState.data && delegatee === stackerState.data.delegatedTo;

  const summarizedStates = delegationStates.map((poolData) => {
    return poolData.stackerStates.reduce(
      (result, stackerState) => {
        console.log(stackerState);
        const isMember = isStillPoolMember(poolData.delegatee, stackerState);
        return {
          delegatee: poolData.delegatee,
          totals: result.totals.add(
            isMember
              ? (stackerState.data.amountUstx as BigNum)
              : new BigNum("0")
          ),
          count: result.count + (isMember ? 1 : 0),
          last: stackerState.stacker,
        };
      },
      {
        delegatee: "",
        totals: new BigNum("0"),
        count: 0,
        last: "",
      }
    );
  });
  writeFileSync(summaryFilePath, JSON.stringify(summarizedStates));
  writeFileSync(statesFilePath, JSON.stringify(delegationStates));

  return delegationStates;
}

export async function stackDelegatedStxsInBatches(
  indices: number[] | undefined,
  length: number,
  rewardPoxAddrCV: ClarityValue,
  startBurnHeight: number,
  lockingPeriod: number,
  minUntilBurnHt: number,
  rewardCycleId: number,
  poolAdmin: { stacks: string; private: string }
) {
  // get delegation states from file and filter
  const delegationStates =
    (await getFilteredDelegationStates(poolAdmin, minUntilBurnHt, true)) || [];
  if (!indices) {
    const numberOfIndices = Math.ceil(delegationStates.length / length);
    indices = [...Array(numberOfIndices).keys()].map((i) => i * length);
  }
  let accountInfo = await accountsApi.getAccountInfo({
    principal: poolAdmin.stacks,
    proof: 0,
  });
  console.log(JSON.stringify(indices));
  for (let i = 0; i < indices.length; i++) {
    let index = indices[i]
    let nonce = accountInfo.nonce + i
    // get slice of member for batched stacking
    // as defined by indices and length
    let members = delegationStates
      .slice(index, index + length)
      .map((state: any) =>
        tupleCV({
          stacker: standardPrincipalCV(state.stacker),
          "amount-ustx": uintCV(
            new BigNum(state.data.amountUstx, "hex").toString(10)
          ),
        })
      );
    // log progress
    console.log(
      index,
      members.length, // count
      cvToString(members[0]) // first in batch
    );
    const membersList = listCV(members);

    // make contract call
    const tx = await makeContractCall({
      contractAddress: poolToolContract.address,
      contractName: poolToolContract.name,
      functionName: "delegate-stack-stx-and-commit",
      functionArgs: [
        membersList,
        rewardPoxAddrCV,
        uintCV(startBurnHeight),
        uintCV(lockingPeriod),
        uintCV(rewardCycleId),
      ],
      senderKey: poolAdmin.private,
      nonce: new BigNum(nonce),
      network: network,
    });

    //console.log(JSON.stringify(tx));
    await handleTransaction(tx);

    // wait for the stacks node to update the nonce
    if (mocknet) {
      await timeout(5000);
    } else {
      //await timeout(120000);      
    }
  }
}
