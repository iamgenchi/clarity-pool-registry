import {
  Configuration,
  InfoApi,
  SmartContractsApi,
} from "@stacks/blockchain-api-client";
import { StackingClient } from "@stacks/stacking";
import {
    ClarityType,
  cvToHex,
  hexToCV,
  noneCV,
  SomeCV,
  standardPrincipalCV,
  TupleCV,
  tupleCV,
  UIntCV,
  uintCV,
} from "@stacks/transactions";
import { network, STACKS_API_URL } from "./deploy";
import { poxContractAddress } from "./pool-tool-utils";
import { poxAddrCVFromBitcoin } from "./utils-pox-addr";
const fetch = require("node-fetch");

const config = new Configuration({
  basePath: STACKS_API_URL,
  fetchApi: fetch,
});
export const infoApi = new InfoApi(config);
const contractsApi = new SmartContractsApi(
  new Configuration({
    basePath: STACKS_API_URL,
    fetchApi: fetch,
  })
);

export async function getPartialStacked(
  poolAdmin: string,
  rewardCycle: number,
  btcAddress: string
) {
  const key = cvToHex(
    tupleCV({
      "pox-addr": poxAddrCVFromBitcoin(btcAddress),
      "reward-cycle": uintCV(rewardCycle),
      sender: standardPrincipalCV(poolAdmin),
    })
  );
  try {
    const amountHex = await contractsApi.getContractDataMapEntry({
      contractAddress: poxContractAddress,
      contractName: "pox",
      mapName: "partial-stacked-by-cycle",
      key,
      proof: 0,
    });
    const amountCV = hexToCV(amountHex.data);
    if (amountCV.type === ClarityType.OptionalNone) {
        return "none"
    } else {
        return (((amountCV as SomeCV).value as TupleCV).data["stacked-amount"] as UIntCV).value.toString(10)
    }
  } catch (e) {
    console.log(e);
  }
}
