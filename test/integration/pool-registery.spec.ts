import { deployContract, mainnet, mocknet } from "../deploy";
import { ADDR1, testnetKeyMap } from "../mocknet";
import * as fs from "fs";
import { expect } from "chai";

export const deployer = mocknet
? testnetKeyMap[ADDR1] :
  JSON.parse(
    fs
      .readFileSync(
        "../../../github/blockstack/stacks-blockchain/keychain2.json"
      )
      .toString()
  )
const poolRegistryContract = mainnet ? "pool-registry" : "pool-registry";

it("deploys", async () => {
  const result = await deployContract(
    `${poolRegistryContract}`,
    "./contracts/pool-registry.clar",
    (s) =>
      mainnet
        ? s.replace(
            /ST000000000000000000002AMW42H/g,
            "SP000000000000000000002Q6VF78"
          )
        : s,
    deployer.private
  );
  expect(result).to.be.a("string");
});
