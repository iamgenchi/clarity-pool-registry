import chai, { expect } from "chai";
import { Client, Provider, ProviderRegistry } from "@blockstack/clarity";
import { ADDR1, ADDR2, ADDR3, ADDR4 } from "../mocknet";
import chaiString from "chai-string";
import * as c32 from "c32check";
import { providerWithInitialAllocations } from "../providerWithInitialAlloc";
import {
  addressHashModeToBtcVersion,
  poxAddrCV,
  poxCVToBtcAddress,
} from "../utils-pox-addr";
import {
  bufferCV,
  BufferCV,
  bufferCVFromString,
  cvToHex,
  cvToJSON,
  cvToString,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { address } from "bitcoinjs-lib";
import { getBtcAddress } from "../pool-tool-utils";
import { cvToValue } from "@stacks/transactions/dist/transactions/src/clarity/clarityValue";

chai.use(chaiString);

describe("pool tool utils", () => {
  it("should create the correct pox address from stx address", async () => {
    const cv = poxAddrCV("SP1K1A1PMGW2ZJCNF46NWZWHG8TS1D23EGH1KNK60");
    expect(cvToString(cv)).equal(
      "(tuple (hashbytes 0x661506d48705f932af21abcff23046b216886e84) (version 0x00))"
    );
  });

  it("should create the correct pox address from multisig btc address", async () => {
    const btcAddress = "3MKGnaKrBZH4ruKCi2tqVveNPuAzfPdLTy";
    const cv = poxAddrCV(c32.b58ToC32(btcAddress));
    expect(cvToString(cv)).equal(
      "(tuple (hashbytes 0xd745c56d7bc89a0d6742a3e65f765ae04aefa7fc) (version 0x01))"
    );
    expect(poxCVToBtcAddress(cv)).equal(btcAddress);
  });

  it("should generate the correct btc address using btc version multisig btc address", async () => {
    const btcAddress = "3HASeAxLLZQJTbGDTSyfxbJXLtnUF8K1Dw";
    const hash = Buffer.from("a9b9c044eefc696e22c204774a5b0aa5d085cad8", "hex");
    expect(address.toBase58Check(hash, 5)).equal(btcAddress);
    expect(
      poxCVToBtcAddress(
        tupleCV({
          hashbytes: bufferCV(hash),
          version: bufferCV(Buffer.from([1])),
        })
      )
    ).equal(btcAddress);
  });

  it("should convert uintCV to hex", async () => {
    const value = cvToJSON(uintCV("1000000000000000"));
    expect(JSON.stringify(value)).equal('{"type":"uint","value":1000000000000000}');
  });
});
