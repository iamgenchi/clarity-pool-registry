# Tools

## Pool Tool Smart Contract

Pool Tool is a smart contract that can be used by pool admins to manage their pool. It allows to
* stack delegated STX in batches for several users at once (`delegate-stack-stx`)
* payout stx (`payout`)
* receive donations (`donate`)

### Stack delegated STX
The function takes as arguments: a list of users and the delegated amount, the reward address, the burn block when to start stacking, the stacking period.

Note, in the deployed "-v1" on mainnet the function is called `delegate-stack-stx-and-commit` and takes also the `reward-cycle` as argument that was only used in "-v0". The "-v1" does not do the commit. "-v0" does include a call to `delegate-aggregation-commit`. However, the network didn't like such a big transaction. Therefore, "-v1" does only stacking, ignoring the reward cycle parameter. The commit call needs to be called manually.

### Payout
The function takes as arguments: the total reward amount, the total stacked amount, a list of users with their stacked amounts.

This function is supposed to distribute to total reward proportionally to the members' stacked amount. This function was not properly tested and it is the responsibility of the pool admin to enter the correct amounts. 

For now, it is recommended to use `send-many-memo` contract instead of this function.

## Pool Tool Utils Javascript

The `test` directory contains a javascript library `pool-too-utils.ts` that supports pool admins to analyse their pool and perform admin tasks.

The section below answer how to use this library to perform certan task
### How much STX has been delegated (before start of reward cylce)?
Users delegate by calling the genesis `pox` contract. If your pool is using a different contract (like xverse-pool) you have to create download and log methods accordingly. Therefore, all tx of the contract needs to be retrieved from a stacks node and then filtered to your needs.

The following downloads the txs and stores them in file `acc-txs-....json` The result is logged to console for easy handling in a spreadsheet app:
```
(async () => {
  await downloadPoxTxs(network);
  logPoxContractCSV(network);
})();
```
Log the console output into a file and open a spreadsheet app. The file has the following fields:
`time,block,function,sender`

Depending on the function the other fields might be empty. They are:
`amount,pool-admin,start-block-ht,locking-cycles,pox-address` 

Now, you can filter by function `delegate-stx` and you find all potential members of your pool. Note, that some users might have revoked delegation by now.

### How to stack the users delegated STXs?
As pool admin you have to call `delegate-stacks-stx` for each user. This function will lock the delegated STXs of the user for the given number of cycles that you as pool admin define. Note, that this can't be longer than the delegation period defined by the user during delegation (if not set as indefinite/`none`).

Pool tools provide a method to do this in batches. Batches of 10 users worked for me. Higher numbers could be possible: 

```
    await stackDelegatedStxsInBatches(
      indices,
      length,
      rewardKeys,
      startBurnHeight,
      lockingPeriod,
      poolAdmin
    );
```

This will go through the list of `indices` and stack users' stx in batches of `length`. The rewards keys are a key file generated from the command line or `stacks-gen sk`. The bitcoin address will be used to as reward address (`pox-addr`). `startBurnHeight` must be before the start of the cycle and `lockingPeriod` is the number of cycles to lock the STXs.

When running, you will see some console log output about the progress. This will take about 15 minutes per batch.

For your accounting, keep track of transactions that failed. Store the delegation states for future references.

### How to document the stacking status (after start of reward cycle)?
If you have used `delegate-stack-stx` of the genesis `pox` contract you can follow the steps from above using `downloadPoxTxs` and `logPoxContractCSV` and then filter by `delegate-stack-stx` and your pool admin address.

If you have used `pool-tool` contract you should use `downloadPoolToolTxs` and `getStackersFromPoolTool` and then group by cycle with `stackersToCycle`:

```
  await downloadPoolToolTxs(network);  
  const stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network
  );
  const cycle4 = stackersToCycle(stackers, 4);
  console.log(JSON.stringify(cycle4));
```
The produced json can be used to publish the data on a website

### How to payout?
Convert the received BTC rewards into STX, substract your fees according to your pool rules and make it public.

The simplest is to prepare a list of user addresses and amounts and then create a `send-many` transaction.

An examples script is given `payout.ts`. Make sure to add a post-condition with the exact amount so that you payout what you promised to payout.

### How to document the payout?

Publish your payout transaction. Users should be able to find their Stacks address in the event list and find the transferred amount.

You could also publish the json of the `stx_transfer` events of the tx. You can find such a transaction at [here](https://stacks-node-api.stacks.co/extended/v1/tx/0xb855ff8858f6942dbc80815b4b143bb5f880f8d293e0871be492cf2c2c506397).

## FAQ
### When to start stacking delegated stx?
When handling transaction by script it is important to get the nonces rights, otherwise some tx can be get stuck pending. Estimate 1 block per tx to be on the safe side. Add 50 blocks extra before the start for the final aggregation commit transaction and if something goes wrong.

### How to find out the number of reward cycles (after the cycle started)?
[Stacking.club](https://stacking.club) gives you the required minimum. `stackersToCycle` gives you the total amount stacked. Dividing the total amount stacked by the required minimum gives you the number of slots. It gives you also an indication of how efficient you were with respect to filling the last reward slot completly.

### When to do the payout?
If the reward cycle has some burn slots before the end of the cycle you could payout during that period such that users can still use the received STX rewards for stacking the next cycle.