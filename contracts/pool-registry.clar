(define-trait pool-trait ((delegate-stx (uint principal (optional uint)
              (optional (tuple (hashbytes (buff 20)) (version (buff 1))))) (response bool int))))
(define-trait pool-trait-ext ((delegate-stx (uint principal (optional uint)
              (optional (tuple (hashbytes (buff 20)) (version (buff 1))))
              (tuple (hashbytes (buff 20)) (version (buff 1)))
              uint) (response bool int))))

(define-constant ERR_INVALID_PAYOUT u1)
(define-constant ERR_NAME_REGISTERED u2)
(define-constant ERR_NAME_NOT_REGISTERED u3)
(define-constant ERR_PERMISSION_DENIED u4)
(define-constant ERR_NATIVE_FUNCTION_FAILED u5)

;; list of registered pools
(define-map registry 
   uint
   (tuple 
      (name (tuple (namespace (buff 20)) (name (buff 48))))
      (pox-address (tuple (hashbytes (buff 20)) (version (buff 1))))
      (url (string-ascii 250))
      (contract (optional principal))
      (extended-contract (optional principal))
      (minimum-ustx (optional uint))
      (locking-period (optional uint))
      (payout (string-ascii 5))))

;; list of means of payout
(define-map payouts
   (string-ascii 5)
   (tuple 
      (name (string-ascii 80) )
   ))

(define-map lookup
   (tuple (namespace (buff 20)) (name (buff 48)))
   uint)
   
(define-data-var last-id uint u0)

(define-private (get-id-by-name? (name (tuple (namespace (buff 20)) (name (buff 48)))))
   (map-get? lookup name))

(define-private (is-owned (name (tuple (namespace (buff 20)) (name (buff 48)))))
   true)

(define-private (to-response (success bool))
   (if success
      (ok true)
      (err {kind: "failure", code: ERR_NATIVE_FUNCTION_FAILED})))

(define-read-only (get-last-id) (var-get last-id))

(define-read-only (get-data-by-name? (name (tuple (namespace (buff 20)) (name (buff 48)))))
 (map-get? registry (unwrap! (get-id-by-name? name) none)))
 
(define-public (add-payout (symbol (string-ascii 5)) (name (string-ascii 80)))
   (ok (map-insert payouts symbol {name: name})))

;; register a new pool that implements the simple pool trait like genesis "pox" contract.
(define-public (register (name (tuple (namespace (buff 20)) (name (buff 48))))
                           (pox-address (tuple (hashbytes (buff 20)) (version (buff 1))))
                           (url (string-ascii 250))
                           (contract <pool-trait>)
                           (minimum-ustx (optional uint))
                           (locking-period (optional uint))
                           (payout (string-ascii 5)))
   (base-register name pox-address url (some (contract-of contract)) none minimum-ustx locking-period payout))

;; register a new pool that implements the extended pool trait
(define-public (register-ext (name (tuple (namespace (buff 20)) (name (buff 48))))
                           (pox-address (tuple (hashbytes (buff 20)) (version (buff 1))))
                           (url (string-ascii 250))
                           (contract <pool-trait-ext>)
                           (minimum-ustx (optional uint))
                           (locking-period (optional uint))
                           (payout (string-ascii 5)))
   (base-register name pox-address url none (some (contract-of contract)) minimum-ustx locking-period payout))

(define-private (base-register (name (tuple (namespace (buff 20)) (name (buff 48))))
                           (pox-address (tuple (hashbytes (buff 20)) (version (buff 1))))
                           (url (string-ascii 250))
                           (contract (optional principal))
                           (extended-contract (optional principal))
                           (minimum-ustx (optional uint))
                           (locking-period (optional uint))
                           (payout (string-ascii 5)))
  (let ((id (+ (get-last-id) u1))
         (payout-details (unwrap! (map-get? payouts payout) (err ERR_INVALID_PAYOUT))))
      (if (and (is-none (map-get? lookup name)) (is-owned name))
         (begin         
            (var-set last-id id) 
            (map-insert registry id 
               {name: name, pox-address: pox-address,
               url: url, 
               contract: contract,
               extended-contract: extended-contract,
               minimum-ustx: minimum-ustx,
               locking-period: locking-period,
               payout: payout}) 
            (map-insert lookup name id)
            (ok id))
         (err ERR_NAME_REGISTERED))))

(define-public (update (name (tuple (namespace (buff 20)) (name (buff 48))))
                           (pox-address (tuple (hashbytes (buff 20)) (version (buff 1))))
                           (url (string-ascii 250))
                           (contract <pool-trait>)
                           (minimum-ustx (optional uint))
                           (locking-period (optional uint))
                           (payout (string-ascii 5)))
   (base-update name pox-address url (some (contract-of contract)) none minimum-ustx locking-period payout))

(define-public (update-ext (name (tuple (namespace (buff 20)) (name (buff 48))))
                           (pox-address (tuple (hashbytes (buff 20)) (version (buff 1))))
                           (url (string-ascii 250))
                           (contract <pool-trait-ext>)
                           (minimum-ustx (optional uint))
                           (locking-period (optional uint))
                           (payout (string-ascii 5)))
   (base-update name pox-address url none (some (contract-of contract)) minimum-ustx locking-period payout))

(define-private (base-update (name (tuple (namespace (buff 20)) (name (buff 48))))
                           (pox-address (tuple (hashbytes (buff 20)) (version (buff 1))))
                           (url (string-ascii 250))
                           (contract (optional principal))
                           (extended-contract (optional principal))
                           (minimum-ustx (optional uint))
                           (locking-period (optional uint))
                           (payout (string-ascii 5)))

   (if (is-owned name)
      (let ((id (unwrap! (get-id-by-name? name) (err {kind: "entry-missing", code: ERR_NAME_NOT_REGISTERED}))))
         (to-response (map-set registry id
            {name: name, 
               pox-address: pox-address,
               url: url, 
               contract: contract,
               extended-contract: extended-contract,
               minimum-ustx: minimum-ustx,
               locking-period: locking-period,
               payout: payout})))
         
      (err {kind: "permission-denied", code: ERR_PERMISSION_DENIED})))

(define-private (get-pool (pool-id uint) (result (list 20 (optional (tuple
   (contract (optional principal))
   (extended-contract (optional principal))
   (locking-period (optional uint))
   (minimum-ustx (optional uint))
   (name (tuple (name (buff 48)) (namespace (buff 20))))
   (payout (string-ascii 5))
   (pox-address (tuple (hashbytes (buff 20)) (version (buff 1))))
   (url (string-ascii 250)))))))
   (unwrap-panic (as-max-len? (append result (map-get? registry pool-id)) u20)))

(define-read-only (get-pools (pool-ids (list 20 uint)))
   (fold get-pool pool-ids (list)))