import {
  bufferCV,
  callReadOnlyFunction,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  makeSTXTokenTransfer,
  standardPrincipalCV,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { user, network, handleTransaction } from "../deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
} from "../pool-tool-utils";

(async () => {
  await downloadPoolToolV0Txs(network);
  await downloadPoolToolTxs(network);
  const v0Stackers = getStackersFromPoolTool(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
  const v1Stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network
  );
  const stackers = v0Stackers.concat(v1Stackers);
  const cycle3 = stackersToCycle(stackers, 3);
  const cycle4 = stackersToCycle(stackers, 4);
  const cycle5 = stackersToCycle(stackers, 5);

  console.log(JSON.stringify(cycle5));
})();
