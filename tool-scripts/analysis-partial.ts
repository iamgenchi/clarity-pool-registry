import { readFileSync } from "fs";
import { mainnet } from "../test/deploy";
import { getPartialStacked } from "../test/pool-tool-partials";

const poolAdmin = JSON.parse(
  readFileSync(
    mainnet ? "../mainnet-keys.json" : "../testnet-keys.json"
  ).toString()
);

const btcAddr1 = "1AJm7XWhbPTFqgtJT6oS9wQ3XThk7rA2yF";

(async () => {
  console.log(await getPartialStacked(poolAdmin.stacks, 5, btcAddr1));
})();
