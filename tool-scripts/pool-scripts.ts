import { readFileSync } from "fs";
import { mainnet } from "../test/deploy";
import {
  downloadPoxTxs,
  logDelegationStatesCSV,
  writeDelegationStates,
} from "../test/pool-tool-utils";

const poolAdmin = JSON.parse(
  readFileSync(
    mainnet ? "../mainnet-keys.json" : "../testnet-keys.json"
  ).toString()
);

(async () => {
  //const txs = await downloadPoxTxs(network);
  //await writeDelegationStates(txs, "");
  console.log("stacker, stacked, amount, admin, untilBurnHt");
  await logDelegationStatesCSV(poolAdmin);
})();
