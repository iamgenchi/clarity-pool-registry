import { cvToString } from "@stacks/transactions";
import { readFileSync } from "fs";
import { mainnet, network } from "../test/deploy";
import {
  downloadPoxTxs,
  infoApi,
  logDelegationStatesCSV,
  stackDelegatedStxsInBatches,
  writeDelegationStates,
} from "../test/pool-tool-utils";
import { poxAddrCV, poxAddrCVFromBitcoin } from "../test/utils-pox-addr";

const rewardPoxAddrCV = mainnet
  ? poxAddrCVFromBitcoin("33WSGLeVoEpuZDjB54HKZ1y5YsERELoVNq")  
  : poxAddrCV(
      JSON.parse(
        readFileSync(
          "../../testnet-keys.json"
        ).toString()
      ).stacks
    );

const poolAdmin = JSON.parse(
  readFileSync(
    mainnet ? "../../mainnet-keys.json" : "../../testnet-keys.json"
  ).toString()
);

(async () => {
  const length = 10;
  // Change here start
  const indices = undefined;
  const lockingPeriod = 5;
  const admin = poolAdmin;
  const minUntilBurnHt = 689050;
  // change here end
  const info = await infoApi.getCoreApiInfo();
  console.log(info.burn_block_height);
  const startBurnHeight = 676300; //info.burn_block_height + 3; // + 1 or more, just in case the node is behind.
  const cycleId = mainnet ? 5 : 124;

  console.log(cvToString(rewardPoxAddrCV));

  await stackDelegatedStxsInBatches(
    indices,
    length,
    rewardPoxAddrCV,
    startBurnHeight,
    lockingPeriod,
    minUntilBurnHt,
    cycleId,
    admin
  );
})();
