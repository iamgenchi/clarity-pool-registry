import {
  bufferCV,
  callReadOnlyFunction,
  FungibleConditionCode,
  listCV,
  makeContractCall,
  makeStandardSTXPostCondition,
  makeSTXTokenTransfer,
  standardPrincipalCV,
  tupleCV,
  uintCV,
} from "@stacks/transactions";
import { user, network, handleTransaction } from "../deploy";
import {
  getStackersFromPoolTool,
  poolToolContract,
  poolToolContractV0,
  downloadPoolToolTxs,
  downloadPoolToolV0Txs,
  stackersToCycle,
} from "../pool-tool-utils";
const BN = require("bn.js");

async function payoutSet(
  stackersSet: { stacker: string; reward: number }[],
  totalPayoutOfSet: number
) {
  const tx = await makeContractCall({
    contractAddress: "SP3FBR2AGK5H9QBDH3EEN6DF8EK8JY7RX8QJ5SVTE",
    contractName: "send-many-memo",
    functionName: "send-many",
    functionArgs: [
      listCV(
        stackersSet.map((s) =>
          tupleCV({
            to: standardPrincipalCV(s.stacker),
            ustx: uintCV(s.reward),
            memo: bufferCV(Buffer.from("reward cycle #4")),
          })
        )
      ),
    ],
    senderKey: user.private,
    network,
    postConditions: [
      makeStandardSTXPostCondition(
        user.stacks,
        FungibleConditionCode.Equal,
        new BN(totalPayoutOfSet)
      ),
    ],
  });
  const result = await handleTransaction(tx);
  console.log({ result });
}

(async () => {
  //await downloadPoolToolV0Txs(network);
  //await downloadPoolToolTxs(network);
  const v0Stackers = getStackersFromPoolTool(
    `${poolToolContractV0.address}.${poolToolContractV0.name}`,
    network
  );
  const v1Stackers = getStackersFromPoolTool(
    `${poolToolContract.address}.${poolToolContract.name}`,
    network
  );
  const stackersInCycle = stackersToCycle(v0Stackers.concat(v1Stackers), 4);
  const stackers = stackersInCycle.members;
  const totalRewards = 13070821829;
  const totalStacked = stackers.reduce((sum: number, s) => sum + s.amount, 0);
  const extendedStackers = stackers.map((stacker) => {
    return {
      reward: Math.round((stacker.amount * totalRewards) / totalStacked),
      ...stacker,
    };
  });
  const totalPayout = extendedStackers.reduce(
    (sum: number, s) => sum + s.reward,
    0
  );
  console.log(extendedStackers.length, totalStacked, totalPayout);

  const set1 = extendedStackers.slice(0, 200);
  const totalPayoutset1 = set1.reduce((sum: number, s) => sum + s.reward, 0);
  const totalStackedSet1 = set1.reduce((sum: number, s) => sum + s.amount, 0);
  console.log(set1.length, totalStackedSet1, totalPayoutset1);
  //await payoutSet(set1, totalPayoutset1);

  const set2 = extendedStackers.slice(200);
  const totalPayoutset2 = set2.reduce((sum: number, s) => sum + s.reward, 0);
  const totalStackedSet2 = set2.reduce((sum: number, s) => sum + s.amount, 0);
  console.log(set2.length, totalStackedSet2, totalPayoutset2);
  await payoutSet(set2, totalPayoutset2);
})();
