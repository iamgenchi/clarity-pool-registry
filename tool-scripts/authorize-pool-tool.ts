import { contractPrincipalCV, makeContractCall, noneCV } from "@stacks/transactions";
import { readFileSync } from "fs";
import { handleTransaction, mainnet, network } from "../test/deploy";
import { poolToolContract, poxContractAddress } from "../test/pool-tool-utils";

const poolAdmin = JSON.parse(
    readFileSync(
      mainnet ? "../../mainnet-keys.json" : "../../testnet-keys.json"
    ).toString()
  );

(async () => {
  const tx = await makeContractCall({
    contractAddress: poxContractAddress,
    contractName: "pox",
    functionName: "allow-contract-caller",
    functionArgs: [
      contractPrincipalCV(poolToolContract.address, poolToolContract.name),
      noneCV(),
    ],
    senderKey: poolAdmin.private,
    network: network,
  });
  await handleTransaction(tx);
})();
